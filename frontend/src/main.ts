import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router'
import LoginVue from './components/Login.vue'
import axios from 'axios'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import { useSessionStore } from './stores/sessionStore'
import '@fortawesome/fontawesome-free/css/all.min.css';
import ReportVue from './sites/Report.vue'
import ImportVue from './sites/Import.vue'
import OpenCourseJobsVue from './sites/OpenCourseJobs.vue'

export const routes =  [
  { path: '/', component: ReportVue, name: 'Kurse melden', adminRoute: false },
  { path: '/login', component: LoginVue, name: 'Login', hideNav: true, adminRoute: false },
  { path: '/import', component: ImportVue, name: 'Import', adminRoute: true },
  { path: '/openCourseJobs', component: OpenCourseJobsVue, name: 'Offene Aufträge', adminRoute: true }
]

const router = createRouter({
    history: createWebHistory(),
    routes: routes
});
const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)

createApp(App).use(router).use(pinia).mount('#app');

const sessionStore = useSessionStore();
console.log(Date.now());
if(sessionStore.exp && Date.now() >= sessionStore.exp*1000) router.push("/login");
axios.defaults.headers.common['Authorization'] = sessionStore.token;
axios.defaults.baseURL = "http://127.0.0.1:9000";