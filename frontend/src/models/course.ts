import { Subject } from "./subject";
import { Teacher } from "./teacher";
import { SchoolClass } from "./schoolclass";

export class Course {
    id!: number;

    subject!: Subject;

    subjectId!: number;

    teacher!: Teacher;

    teacherId!: number;

    schoolClass!: SchoolClass;

    schoolClassId!: number;

    numberOfhours!: number;

    processed!: boolean;

    updatedAt!: boolean;

    updatedBy!: Teacher;

    deleted!: boolean;
}