import { TypeOfSchool } from "./typeofschool";
import { Course } from "./course";

export class SchoolClass {
    id!: number;

    name!: string;

    courses!: Course[];

    typeOfSchool!: TypeOfSchool;
}