import { Course } from "./course";

export class Teacher {
    id!: number;

    firstname!: string;

    lastname!: string;

    short!: string;

    isAdmin!: string;

    password!: string | null;

    courses!: Course[];
}