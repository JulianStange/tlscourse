import { SchoolClass } from "./schoolclass";

export class TypeOfSchool {
    id!: number;

    name!: string;

    schoolClasses!: SchoolClass[];
}