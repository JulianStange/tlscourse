import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useSessionStore = defineStore('sessionStore', () => {
    const loggedIn = ref(false);
    const teacherId = ref(0);
    const token = ref("");
    const isAdmin = ref(false);
    const exp = ref<number>(0);

    return { loggedIn, teacherId,token, isAdmin, exp }
  },
  {
    persist: true
  }
)