import * as dotenv from "dotenv";
import express, { NextFunction } from "express";
import cors from 'cors';
import { pagination } from "typeorm-pagination";
import { AppDataSource } from "./dataSource";
import { SchoolClass } from "./entity/schoolclass";
import fs from 'fs';
import jwt from 'jsonwebtoken';
import { Teacher } from "./entity/teacher";
import { TypeOfSchool } from "./entity/typeofschool";
import { Course } from "./entity/course";
import { Subject } from "./entity/subject";
import bodyParser from "body-parser";
import { JwtPayload } from "./interface/jwtPayload";

dotenv.config();
const PORT = process.env.PORT || 9000;
const app = express();
const privateKey = fs.readFileSync('private.key');
app.use(cors({
    origin: '*'
  }));
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));
app.use(express.json());
app.use(pagination);

const checkToken = (req: any, res: any, next: NextFunction) => {
    var token = req.headers.authorization;
    if (token && jwt.verify(token, privateKey))
    {
        next();
    }
    else
    {
        res.status(401);
    }
}
const isLoggedInAsAdmin = (req: any, res: any, next: NextFunction) => {
    var token = req.headers.authorization;
    const decoded = jwt.verify(token, privateKey) as JwtPayload;
    const teacherRepo = AppDataSource.getRepository(Teacher).findOne({'where': {id: decoded.data.teacherId}});
    if (token && decoded && teacherRepo != null)
    {
        next();
    }
    else
    {
        res.status(401);
    }
}
app.post("/import/classinformations", isLoggedInAsAdmin , async (req, res) => {
    const typeOfSchoolRepo = AppDataSource.getRepository(TypeOfSchool);
    const schoolclassRepo = AppDataSource.getRepository(SchoolClass);
    const buffedInput = Buffer.from(req.body.file.split(",")[1], 'base64');
    const fileContent = buffedInput.toString('binary');
    const lines = fileContent.split(/\r?\n/);

    var imported = 0;
    for (var i = 1; i < lines.length;i++) {
        const cols = lines[i].split(";");
        const schoolclass = new SchoolClass();
        const schoolTypeName = cols[7];
        const schoolClassName = cols[0].trim();
        if(!schoolClassName.includes("FOS") && !schoolClassName.includes("BG")){
            if(schoolClassName && (await schoolclassRepo.findOne({where: {name: schoolClassName}}) == null)){
                imported++;
                schoolclass.name = schoolClassName;
                var typeOfSchool = await typeOfSchoolRepo.findOne({where: {name: schoolTypeName}});
                if(typeOfSchool == null) {
                    typeOfSchool = new TypeOfSchool();
                    typeOfSchool.name = schoolTypeName;
                }
                schoolclass.typeOfSchool = typeOfSchool;
                schoolclassRepo.save(schoolclass);
            }
        }
    }
    res.status(200).json({
        'schoolClassImportedCount': imported
    });
});
app.post("/import/teacher", checkToken, isLoggedInAsAdmin, async (req, res) => {
    const teacherRepo = AppDataSource.getRepository(Teacher);
    const buffedInput = Buffer.from(req.body.file.split(",")[1], 'base64');
    const fileContent = buffedInput.toString('binary');
    const lines = fileContent.split(/\r?\n/);

    var imported = 0;
    for (var i = 1; i < lines.length;i++) {
        const cols = lines[i].split(";");
        const firstname = cols[0].split(" ")[0];
        const lastname = cols[1];
        const short = cols[2];
        if(firstname && lastname && short != null && (await teacherRepo.findOne({where: {short: short}}) == null)){
            imported++;
            const teacher = new Teacher();
            teacher.firstname = firstname;
            teacher.lastname = lastname;
            teacher.short = short;
            teacherRepo.save(teacher);
        }
    }
    res.status(200).json({
        'importedCount': imported
    });
});
app.post("/import/course", isLoggedInAsAdmin, async (req, res) => {
    const subjectRepo = AppDataSource.getRepository(Subject);
    const courseRepo = AppDataSource.getRepository(Course);
    const teacherRepo = AppDataSource.getRepository(Teacher);
    const classRepo = AppDataSource.getRepository(SchoolClass);
    const buffedInput = Buffer.from(req.body.file.split(",")[1], 'base64');
    const fileContent = buffedInput.toString('binary');
    const lines = fileContent.split(/\r?\n/);

    AppDataSource.query('TRUNCATE TABLE course');
    var imported = 0;
    for (var i = 1; i < lines.length;i++) {
        const cols = lines[i].split(";");
        const schoolType = cols[9];
        if(schoolType && !schoolType.startsWith("BG") && !schoolType.startsWith("FOS")){
            const subjectShort = cols[2].trim();
            const className = cols[15].trim();
            const teacherShort = cols[18].trim();
            const hours = Number(cols[7].replace(",", "."));
            const teacher = await teacherRepo.findOne({'where': {'short': teacherShort}});
            const schoolClass = await classRepo.findOne({'where': {'name': className}});
            let subject = await subjectRepo.findOne({'where': {'name': subjectShort}});
            if(subject == null){
                subject = new Subject();
                subject.name = subjectShort;
                await subjectRepo.insert(subject);
            }
            if(teacher && schoolClass && subject.name != ''){
                imported++;
                const course = new Course();
                course.subject = subject;
                course.teacher = teacher;
                course.schoolClass = schoolClass;
                course.numberOfhours = hours;
                course.processed = true;
                courseRepo.save(course);
            }
        }
    }
    res.status(200).json({
        'importedCount': imported
    });
});

//return all SchoolClasses
app.get("/schoolclasses", checkToken, async (_, res) => {
    const schoolClassRepository = AppDataSource.getRepository(SchoolClass);
    res.status(200)
        .json((await schoolClassRepository.find({'order': {'name': 'ASC'}})));
});

//returns all teachers
app.get("/teachers", checkToken, async (_, res) => {
    const teacherRepository = AppDataSource.getRepository(Teacher);
    res.status(200)
        .json((await teacherRepository.find({order: {lastname: "ASC"}})));
});

//return coursefrom for a specific schoolclass
app.get("/courseform/:id", checkToken, async (req, res) => {
    const subs = await AppDataSource.getRepository(Subject)
        .createQueryBuilder("subject")
        .leftJoinAndSelect("subject.courses", "courses", "courses.schoolClassId = :id AND courses.deleted = 0", {id: req.params.id})
        .getMany();
    res.status(200)
        .json(subs);
});

//save courseform
app.put("/courseform", checkToken, async (req, res) => {
    for(const x of req.body){
        await AppDataSource.getRepository(Course).save(x.courses);
    }
    res.status(200)
        .send("success");
});

//close open course jobs
app.get("/closeCourseJob/:id", isLoggedInAsAdmin, async (req, res) => {
    await AppDataSource.getRepository(Course).update({id: Number(req.params.id)}, {processed: true});
    res.status(200)
        .send("success");
});

//view open course jobs
app.get("/openCourseJobs", isLoggedInAsAdmin, async (_, res) => {
    console.log("test");
    const opend = await AppDataSource.getRepository(TypeOfSchool)
        .createQueryBuilder("typeOfSchool")
        .leftJoinAndSelect("typeOfSchool.schoolClasses", "schoolClass")
        .leftJoinAndSelect("schoolClass.courses", "course", "course.processed = false")
        .leftJoinAndSelect("course.subject", "subject")
        .leftJoinAndSelect("course.teacher", "teacher")
        .where(qb => {
            const subQuery = qb.subQuery()
                .select("schoolClass.id")
                .from(SchoolClass, "schoolClass")
                .leftJoin("schoolClass.courses", "course", "course.processed = false")
                .where("course.id IS NOT NULL")
                .getQuery();
            return "schoolClass.id IN " + subQuery;
        })
        .getMany();
    console.log(opend);
    res.status(200)
        .send(opend);
});

//login and return token
app.post("/login",  async (req, res) => {
    const teacherRepository = AppDataSource.getRepository(Teacher);
    const teacher = await teacherRepository.findOne({where: {short: req.body.data.username}});
    const password = req.body.data.password;
    if(teacher){
        if(teacher.password != null && teacher.password == password || password == "TLS"){
            const exp = Math.floor(Date.now() / 1000) + (60 * 60);
            var token = jwt.sign({ exp: exp,data: { teacherId: teacher.id} }, privateKey);
            res.status(200)
                .json({
                    success: true,
                    token: token,
                    exp: exp,
                    teacherId: teacher.id,
                    isAdmin: teacher.isAdmin
                });
            return;
        }
    }
    res.status(401)
        .json({
            success: false
        });
});

AppDataSource.initialize().then(() => {
    app.listen(PORT, () => {
        console.log(`CONNECTED TO DB AND SERVER START ON ${PORT}`);
      });
});

