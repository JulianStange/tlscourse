import { DataSource } from "typeorm";

export const AppDataSource = new DataSource({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "",
    //DB name schould by lowercase cause synchronize error
    database: "tlscourse",
    dropSchema: false,
    synchronize: true,
    entities: [
        "entity/*{.ts,.js}"
    ]
})