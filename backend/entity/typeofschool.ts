import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, OneToMany } from "typeorm"
import { SchoolClass } from "./schoolclass";

@Entity()
export class TypeOfSchool {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name!: string;

    @OneToMany(() => SchoolClass, (schoolClass) => schoolClass.typeOfSchool)
    schoolClasses!: SchoolClass[];
}