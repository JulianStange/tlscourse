import { Entity, PrimaryGeneratedColumn, Column, OneToMany, JoinColumn, ManyToOne } from "typeorm"
import { TypeOfSchool } from "./typeofschool";
import { Course } from "./course";

@Entity()
export class SchoolClass {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name!: string;

    @OneToMany(() => Course, (c) => c.schoolClass)
    courses!: Course[];

    @ManyToOne(() => TypeOfSchool, (typeOfSchool) => typeOfSchool.schoolClasses, {cascade: true})
    typeOfSchool!: TypeOfSchool;
}