import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, UpdateDateColumn, BeforeUpdate, JoinColumn } from "typeorm"
import { Subject } from "./subject";
import { Teacher } from "./teacher";
import { SchoolClass } from "./schoolclass";

@Entity()
export class Course {
    @PrimaryGeneratedColumn()
    id!: number;

    @ManyToOne(() => Subject, (subject) => subject.courses, {cascade: true, nullable: false})
    subject!: Subject;

    @Column()
    subjectId!: number;

    @ManyToOne(() => Teacher, (t) => t.courses)
    teacher!: Teacher;

    @Column()
    teacherId!: number;

    @ManyToOne(() => SchoolClass)
    schoolClass!: SchoolClass;

    @Column()
    schoolClassId!: number;

    @Column()
    numberOfhours!: number;

    @Column()
    processed!: boolean;

    @Column({ default: false })
    deleted!: boolean;

    @UpdateDateColumn()
    updatedAt!: boolean;

    @ManyToOne(() => Teacher)
    updatedBy!: Teacher;
}