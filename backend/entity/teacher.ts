import { Entity, PrimaryGeneratedColumn, Column, Unique, OneToMany } from "typeorm"
import { Course } from "./course";

@Entity({name: 'teacher', schema: 'public' })
@Unique('short', ['short'])
export class Teacher {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    firstname!: string;

    @Column()
    lastname!: string;

    @Column()
    short!: string;

    @Column()
    isAdmin!: string;

    @Column({nullable: true, type:"nvarchar"})
    password!: string | null;

    @OneToMany(() => Course, (c) => c.teacher)
    courses!: Course[];
}