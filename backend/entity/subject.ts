import { Entity, PrimaryGeneratedColumn, Column, OneToMany, Unique, JoinColumn } from "typeorm"
import { Course } from "./course";

@Entity()
@Unique('name', ['name'])
export class Subject {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name!: string;

    @OneToMany(() => Course, (c) => c.subject, {onDelete: 'CASCADE'})
    @JoinColumn({
        name: 'subjectId',
        referencedColumnName: 'subjectId'
    })
    courses!: Course[];
}