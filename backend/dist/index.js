"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = __importStar(require("dotenv"));
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const typeorm_pagination_1 = require("typeorm-pagination");
const dataSource_1 = require("./dataSource");
const schoolclass_1 = require("./entity/schoolclass");
const fs_1 = __importDefault(require("fs"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const teacher_1 = require("./entity/teacher");
const typeofschool_1 = require("./entity/typeofschool");
dotenv.config();
const PORT = process.env.PORT || 9000;
const app = (0, express_1.default)();
const privateKey = fs_1.default.readFileSync('private.key');
app.use((0, cors_1.default)({
    origin: '*'
}));
app.use(express_1.default.urlencoded({ extended: true }));
app.use(express_1.default.json());
app.use(typeorm_pagination_1.pagination);
const checkToken = (req, res, next) => {
    var token = req.headers.authorization;
    if (token && jsonwebtoken_1.default.verify(token, privateKey)) {
        next();
    }
    else {
        res.status(401);
    }
};
app.post("/import/classinformations", checkToken, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const typeOfSchoolRepo = dataSource_1.AppDataSource.getRepository(typeofschool_1.TypeOfSchool);
    const schoolclassRepo = dataSource_1.AppDataSource.getRepository(schoolclass_1.SchoolClass);
    const buffedInput = Buffer.from(req.body.file, 'base64');
    const fileContent = buffedInput.toString('utf-8');
    const lines = fileContent.split(/\r?\n/);
    for (var i = 1; i > lines.length; i++) {
        const cols = lines[i].split(";");
        const schoolclass = new schoolclass_1.SchoolClass();
        const schoolTypeName = cols[7];
        schoolclass.name = cols[0];
        var typeOfSchool = yield typeOfSchoolRepo.findOne({ where: { name: schoolTypeName } });
        if (typeOfSchool == null) {
            typeOfSchool = new typeofschool_1.TypeOfSchool();
            typeOfSchool.name = schoolTypeName;
        }
        schoolclass.typeOfSchool = typeOfSchool;
        schoolclassRepo.save(schoolclass);
    }
    res.status(200);
}));
app.get("/schoolclasses", checkToken, (_, res) => __awaiter(void 0, void 0, void 0, function* () {
    const vehicleRepository = dataSource_1.AppDataSource.getRepository(schoolclass_1.SchoolClass);
    res.status(200)
        .json((yield vehicleRepository.find()));
}));
app.post("/login", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const teacherRepository = dataSource_1.AppDataSource.getRepository(teacher_1.Teacher);
    const teacher = yield teacherRepository.findOne({ where: { short: req.body.data.username } });
    const password = req.body.data.password;
    if (teacher) {
        if (teacher.password != null && teacher.password == password || password == "TLS") {
            const exp = Math.floor(Date.now() / 1000) + (60 * 60);
            var token = jsonwebtoken_1.default.sign({ exp: exp, date: { teacherId: teacher.id } }, privateKey);
            res.status(200)
                .json({
                success: true,
                token: token,
                exp: exp,
                teacherId: teacher.id,
                isAdmin: teacher.isAdmin
            });
            return;
        }
    }
    res.status(401)
        .json({
        success: false
    });
}));
dataSource_1.AppDataSource.initialize().then(() => {
    app.listen(PORT, () => {
        console.log(`CONNECTED TO DB AND SERVER START ON ${PORT}`);
    });
});
