"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TypeOfSchool = void 0;
const typeorm_1 = require("typeorm");
const schoolclass_1 = require("./schoolclass");
let TypeOfSchool = class TypeOfSchool {
};
exports.TypeOfSchool = TypeOfSchool;
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], TypeOfSchool.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], TypeOfSchool.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => schoolclass_1.SchoolClass, (schoolClass) => schoolClass.typeOfSchool),
    __metadata("design:type", Array)
], TypeOfSchool.prototype, "schoolClasses", void 0);
exports.TypeOfSchool = TypeOfSchool = __decorate([
    (0, typeorm_1.Entity)({ 'name': 'type_of_school' })
], TypeOfSchool);
